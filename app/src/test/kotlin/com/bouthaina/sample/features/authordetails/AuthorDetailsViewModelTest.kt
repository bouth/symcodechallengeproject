
package com.bouthaina.sample.features.authordetails

import com.bouthaina.sample.AndroidTest
import com.bouthaina.sample.core.data.model.Address
import com.bouthaina.sample.core.data.model.Author
import com.bouthaina.sample.core.data.model.Post
import com.bouthaina.sample.core.functional.Either
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.given
import kotlinx.coroutines.experimental.runBlocking
import org.amshove.kluent.shouldEqualTo
import org.junit.Before
import org.junit.Test

import org.mockito.Mock

class AuthorDetailsViewModelTest : AndroidTest() {

    private lateinit var authorDetailsViewModel: AuthorDetailsViewModel

    @Mock private lateinit var getAuthorDetails: GetAuthorDetails
    @Mock private lateinit var address: Address
    @Mock private lateinit var author: Author
    @Mock private lateinit var post1: Post
    @Mock private lateinit var post2: Post
    @Mock private lateinit var postList: ArrayList<Post>


    @Before
    fun setUp() {
        address.longitude = "111"
        address.latitude = "558"
        author.address = address
        author.id = 1
        author.name = "Roberto"
        author.email = "jjgg@ggg.com"

        post1.authorId = 1
        post1.title ="bouthaina"
        post1.date ="bouthaina is ready now"
        post1.id =1

        post2.authorId = 1
        post2.title ="bouthaina"
        post2.date ="bouthaina is ready now"
        post2.id =1

        postList = ArrayList<Post>()
        postList.add(post1)
        postList.add(post2)

        authorDetailsViewModel = AuthorDetailsViewModel(getAuthorDetails)
    }

    @Test fun `loading posts  should update live data`() {
        val authorDetails = author
        given { runBlocking { getAuthorDetails.run(eq(any())) } }.willReturn(Either.Right(postList))

        authorDetailsViewModel.postsList.observeForever {
            with(it!!) { it == postList }
        }

       runBlocking { authorDetailsViewModel.loadAuthorDetails(1) }
    }
}
