package com.bouthaina.sample.core.data.model

import android.os.Parcel
import com.bouthaina.sample.core.extension.empty

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

data class Author(var id: Int, var name: String , var userName: String ,var email: String ,var avatarUrl: String , var address : Address) {

    companion object {
        fun empty() = Author(0, String.empty(), String.empty(), String.empty(), String.empty(), Address.createFromParcel(Parcel.obtain()))
    }
}
