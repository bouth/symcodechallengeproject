
package com.bouthaina.sample.core.platform

import android.content.Context
import com.bouthaina.sample.core.extension.networkInfo
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 *
 *
 * Injectable class which returns information about the network connection state.
 */
@Singleton
class NetworkHandler
@Inject constructor(private val context: Context) {
    val isConnected get() = context.networkInfo?.isConnectedOrConnecting
}