
package com.bouthaina.sample.core.data.network

import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

@Singleton
class AuthorsService
@Inject constructor(retrofit: Retrofit) : AuthorsApi {

    private val authorsApi by lazy { retrofit.create(AuthorsApi::class.java) }
    override fun authors() = authorsApi.authors()
    override fun posts(postId: Int) = authorsApi.posts(postId)
    override fun postsAllList() = authorsApi.postsAllList()
}
