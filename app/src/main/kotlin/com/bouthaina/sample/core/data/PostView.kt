package com.bouthaina.sample.core.data

import android.os.Build
import android.os.Parcel
import android.support.annotation.RequiresApi
import com.bouthaina.sample.core.platform.KParcelable
import com.bouthaina.sample.core.platform.parcelableCreator

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

data class PostView(val id: Int,
                    val title: String,
                    val date: String,
                    val body: String,
                    val imageUrl: String,
                    val authorId: Int): KParcelable {
    companion object {
        @JvmField val CREATOR = parcelableCreator(::PostView)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    constructor(parcel: Parcel) : this(parcel.readInt(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString() ,parcel.readInt())

    override fun writeToParcel(dest: Parcel, flags: Int) {
        with(dest) {
            writeInt(id)
            writeString(title)
            writeString(date)
            writeString(body)
            writeString(imageUrl)
            writeInt(authorId)
        }
    }
}
