
package com.bouthaina.sample.core.data.model

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

data class AuthorEntity(val id: Int, val name: String , val userName: String ,val email: String ,val avatarUrl: String ,val address: Address ) {
    fun toAuthor() = Author(id, name, userName, email, avatarUrl,address)
}
