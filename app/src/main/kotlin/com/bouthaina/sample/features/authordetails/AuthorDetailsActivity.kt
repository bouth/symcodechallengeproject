
package com.bouthaina.sample.features.authordetails

import android.content.Context
import android.content.Intent
import com.bouthaina.sample.core.data.AuthorView
import com.bouthaina.sample.core.platform.BaseActivity

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

class AuthorDetailsActivity : BaseActivity() {

    companion object {
        private const val INTENT_EXTRA_PARAM_AUTHOR = "com.bouthaina.INTENT_PARAM_AUTHOR "

        fun callingIntent(context: Context, author: AuthorView): Intent {
            val intent = Intent(context, AuthorDetailsActivity::class.java)
            intent.putExtra(INTENT_EXTRA_PARAM_AUTHOR, author)
            return intent
        }
    }

    override fun fragment() = AuthorDetailsFragment.forAuthor(intent.getParcelableExtra(INTENT_EXTRA_PARAM_AUTHOR))
}
