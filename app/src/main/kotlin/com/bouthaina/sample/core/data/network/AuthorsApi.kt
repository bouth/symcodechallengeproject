package com.bouthaina.sample.core.data.network

import com.bouthaina.sample.core.data.model.AuthorEntity
import com.bouthaina.sample.core.data.model.PostEntity
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

internal interface AuthorsApi {
    companion object {
        private const val PARAM_AUTHOR_ID = "authorId"
        private const val AUTHORS = "authors"
        private const val POSTS = "posts"
        private const val PARAM_GET_AUTHOR_DETAILS = "posts?authodId=authorId"
    }

    @GET(AUTHORS)
    fun authors(): Call<List<AuthorEntity>>

    @GET(PARAM_GET_AUTHOR_DETAILS)
    fun posts(@Query(PARAM_AUTHOR_ID) postId: Int): Call<List<PostEntity>>

    @GET(POSTS)
    fun postsAllList(): Call<List<PostEntity>>
}
