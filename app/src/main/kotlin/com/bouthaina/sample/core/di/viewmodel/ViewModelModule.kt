
package com.bouthaina.sample.core.di.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.bouthaina.sample.features.authordetails.AuthorDetailsViewModel
import com.bouthaina.sample.features.authors.AuthorsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

@Module
abstract class ViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(AuthorsViewModel::class)
    abstract fun bindsAuthorsViewModel(authorsViewModel: AuthorsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AuthorDetailsViewModel::class)
    abstract fun bindsAuthorDetailsViewModel(authorDetailsViewModel: AuthorDetailsViewModel): ViewModel
}