
package com.bouthaina.sample.features.authordetails

import com.bouthaina.sample.UnitTest
import com.bouthaina.sample.core.data.model.Address
import com.bouthaina.sample.core.data.model.Author
import com.bouthaina.sample.features.authors.GetAuthors
import com.bouthaina.sample.core.functional.Either.Right
import com.bouthaina.sample.core.interactor.UseCase
import com.bouthaina.sample.core.data.network.AuthorsRepository
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

class GetAuthorsTest : UnitTest() {

    private lateinit var getauthors: GetAuthors

    @Mock private lateinit var authorsRepository: AuthorsRepository
    @Mock private lateinit var address: Address
    @Mock private lateinit var author: Author


    @Before fun setUp() {
        getauthors = GetAuthors(authorsRepository)
        address.longitude = "111"
        address.latitude = "558"
        author.address = address
        author.id = 1
        author.name = "Roberto"
        author.email = "jjgg@ggg.com"

        given { authorsRepository.authors() }.willReturn(Right(listOf(author)))
    }

    @Test fun `should get data from repository`() {
        runBlocking { getauthors.run(UseCase.None()) }

        verify(authorsRepository).authors()
        verifyNoMoreInteractions(authorsRepository)
    }
}
