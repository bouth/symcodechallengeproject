
package com.bouthaina.sample.features.authordetails

import android.arch.lifecycle.MutableLiveData
import com.bouthaina.sample.core.data.model.Post
import com.bouthaina.sample.core.data.PostView
import com.bouthaina.sample.features.authordetails.GetAuthorDetails.Params
import com.bouthaina.sample.core.platform.BaseViewModel
import javax.inject.Inject

class AuthorDetailsViewModel
@Inject constructor(private val getAuthorDetails: GetAuthorDetails) : BaseViewModel() {

    var postsList: MutableLiveData<List<PostView>> = MutableLiveData()

    fun loadAuthorDetails(authorId: Int) =
            getAuthorDetails(Params(authorId)) { it.either(::handleFailure, ::handlePosts) }

    private fun handlePosts(posts: List<Post>) {
        this.postsList.value = posts.map { PostView(it.id, it.title, it.date, it.body, it.imageUrl, it.authorId) }
    }
}