package com.bouthaina.sample.core.data.model

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

data class PostEntity(val id: Int, val title: String, val date: String, val body: String, val imageUrl: String, val authorId: Int ) {
    fun toPost() = Post(id, title, date, body, imageUrl , authorId)
}
