package com.bouthaina.sample.features.authordetails

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup


import javax.inject.Inject

import android.support.v7.widget.RecyclerView
import com.bouthaina.sample.core.data.PostView

import com.bouthaina.sample.core.extension.inflate
import com.bouthaina.sample.core.extension.loadFromUrl
import com.bouthaina.sample.core.navigation.Navigator
import kotlinx.android.synthetic.main.post_row.view.*
import java.text.ParseException
import java.text.SimpleDateFormat

import java.util.*
import kotlin.properties.Delegates



/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

class PostAdapter
@Inject constructor() : RecyclerView.Adapter<PostAdapter.ViewHolder>() {

    internal var collection: List<PostView> by Delegates.observable(emptyList()) {
        _, _, _ -> notifyDataSetChanged()
    }

    internal var clickListener: (PostView, Navigator.Extras) -> Unit = { _, _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(parent.inflate(com.bouthaina.sample.R.layout.post_row))

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) =
            viewHolder.bind(collection[position], clickListener)

    override fun getItemCount() = collection.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SimpleDateFormat")
        fun bind(post: PostView, clickListener: (PostView, Navigator.Extras) -> Unit) {

            val fmt =  SimpleDateFormat("yyyy-MM-dd")
            val fmt2 = SimpleDateFormat("dd-MM-yyyy")
            val date = try {fmt2.format(fmt.parse(post.date))} catch(pe : ParseException) {""}

            itemView.postImage.loadFromUrl(post.imageUrl)
            itemView.postTitle.text = post.title
            itemView.postDescription.text = date
            itemView.setOnClickListener { clickListener(post, Navigator.Extras(itemView.layoutItem)) }

        }
    }
}
