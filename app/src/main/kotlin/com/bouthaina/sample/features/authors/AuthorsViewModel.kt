
package com.bouthaina.sample.features.authors

import android.arch.lifecycle.MutableLiveData
import com.bouthaina.sample.core.data.AuthorView
import com.bouthaina.sample.core.interactor.UseCase.None
import com.bouthaina.sample.core.data.model.Author
import com.bouthaina.sample.core.platform.BaseViewModel
import javax.inject.Inject

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

class AuthorsViewModel
@Inject constructor(private val getAuthors: GetAuthors) : BaseViewModel() {

    var authors: MutableLiveData<List<AuthorView>> = MutableLiveData()

    fun loadAuthors() = getAuthors(None()) { it.either(::handleFailure, ::handleAuthorList) }

    private fun handleAuthorList(authors: List<Author>) {
        this.authors.value = authors.map { AuthorView(it.id, it.name, it.userName, it.email, it.avatarUrl , it.address) }
    }
}