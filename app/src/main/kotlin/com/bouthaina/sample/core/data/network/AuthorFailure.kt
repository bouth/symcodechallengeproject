
package com.bouthaina.sample.core.data.network

import com.bouthaina.sample.core.exception.Failure.FeatureFailure

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

class AuthorFailure {
    class ListNotAvailable: FeatureFailure()
    class NonExistentAuthor: FeatureFailure()
}

