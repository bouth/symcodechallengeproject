
package com.bouthaina.sample.features.authors

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bouthaina.sample.R
import com.bouthaina.sample.core.data.AuthorView
import com.bouthaina.sample.core.extension.inflate
import com.bouthaina.sample.core.extension.loadFromUrl
import com.bouthaina.sample.core.navigation.Navigator
import kotlinx.android.synthetic.main.row_author.view.*
import javax.inject.Inject
import kotlin.properties.Delegates
/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

class AuthorsAdapter
@Inject constructor() : RecyclerView.Adapter<AuthorsAdapter.ViewHolder>() {

    internal var collection: List<AuthorView> by Delegates.observable(emptyList()) {
        _, _, _ -> notifyDataSetChanged()
    }

    internal var clickListener: (AuthorView, Navigator.Extras) -> Unit = { _, _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(parent.inflate(R.layout.row_author))

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) =
            viewHolder.bind(collection[position], clickListener)

    override fun getItemCount() = collection.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(authorView: AuthorView, clickListener: (AuthorView, Navigator.Extras) -> Unit) {
            itemView.authorPoster.loadFromUrl(authorView.avatarUrl)
            itemView.setOnClickListener { clickListener(authorView, Navigator.Extras(itemView.authorPoster)) }
        }
    }
}
