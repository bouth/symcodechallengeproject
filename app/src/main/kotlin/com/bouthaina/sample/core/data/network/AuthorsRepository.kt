
package com.bouthaina.sample.core.data.network

import com.bouthaina.sample.core.exception.Failure
import com.bouthaina.sample.core.exception.Failure.NetworkConnection
import com.bouthaina.sample.core.exception.Failure.ServerError
import com.bouthaina.sample.core.functional.Either
import com.bouthaina.sample.core.functional.Either.Left
import com.bouthaina.sample.core.functional.Either.Right
import com.bouthaina.sample.core.data.model.Author
import com.bouthaina.sample.core.platform.NetworkHandler
import com.bouthaina.sample.core.data.model.Post
import retrofit2.Call
import javax.inject.Inject

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

interface AuthorsRepository {
    fun authors(): Either<Failure, List<Author>>
    fun posts(authoId: Int): Either<Failure, List<Post>>

    class Network
    @Inject constructor(private val networkHandler: NetworkHandler,
                        private val service: AuthorsService) : AuthorsRepository {

        override fun authors(): Either<Failure, List<Author>> {
            return when (networkHandler.isConnected) {
                true -> request(service.authors(), { it.map { it.toAuthor() } }, emptyList())
                false, null -> Left(NetworkConnection)
            }
        }

        override fun posts(authoId: Int): Either<Failure, List<Post>> {
            return when (networkHandler.isConnected) {
                true -> request(service.posts(authoId), { it.map { it.toPost() }},  emptyList())
                false, null -> Left(NetworkConnection)
            }
        }
        private fun <T, R> request(call: Call<T>, transform: (T) -> R, default: T): Either<Failure, R> {
            return try {
                val response = call.execute()
                when (response.isSuccessful) {
                    true -> Right(transform((response.body() ?: default)))
                    false -> Left(ServerError)
                }
            } catch (exception: Throwable) {
                Left(ServerError)
            }
        }
    }
}
