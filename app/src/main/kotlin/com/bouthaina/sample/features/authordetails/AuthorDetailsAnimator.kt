
package com.bouthaina.sample.features.authordetails

import android.support.v4.app.FragmentActivity
import android.support.v4.view.animation.FastOutSlowInInterpolator
import android.transition.Fade
import android.transition.TransitionManager
import android.view.View
import android.view.ViewGroup
import com.bouthaina.sample.core.extension.cancelTransition
import javax.inject.Inject


/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

class AuthorDetailsAnimator
@Inject constructor() {

    private val TRANSITION_DELAY = 200L
    private val TRANSITION_DURATION = 400L

    internal fun postponeEnterTransition(activity: FragmentActivity) = activity.postponeEnterTransition()
    internal fun cancelTransition(view: View) = view.cancelTransition()

    internal fun fadeVisible(viewContainer: ViewGroup, view: View) = beginTransitionFor(viewContainer, view, View.VISIBLE)
    internal fun fadeInvisible(viewContainer: ViewGroup, view: View) = beginTransitionFor(viewContainer, view, View.INVISIBLE)

    private fun beginTransitionFor(viewContainer: ViewGroup, view: View, visibility: Int) {
        val transition = Fade()
        transition.startDelay = TRANSITION_DELAY
        transition.duration = TRANSITION_DURATION
        TransitionManager.beginDelayedTransition(viewContainer, transition)
        view.visibility = visibility
    }
}


