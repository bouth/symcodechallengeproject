
package com.bouthaina.sample.features.authordetails

import com.bouthaina.sample.core.data.model.Post
import com.bouthaina.sample.features.authordetails.GetAuthorDetails.Params
import com.bouthaina.sample.core.interactor.UseCase
import com.bouthaina.sample.core.data.network.AuthorsRepository
import javax.inject.Inject

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

class GetAuthorDetails
@Inject constructor(private val authorsRepository: AuthorsRepository) : UseCase<List<Post>, Params>() {

    override suspend fun run(params: Params) = authorsRepository.posts(params.id)

    data class Params(val id: Int)
}
