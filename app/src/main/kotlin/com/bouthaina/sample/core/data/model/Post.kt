package com.bouthaina.sample.core.data.model

import com.bouthaina.sample.core.extension.empty

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

data class Post(var id: Int,
                var title: String,
                var date: String,
                var body: String,
                var imageUrl: String,
                var authorId: Int) {

    companion object {
        fun empty() = Post(0, String.empty(), String.empty(), String.empty(),
                String.empty(), 0)
    }
}
