
package com.bouthaina.sample.core.di

import com.bouthaina.sample.AndroidApplication
import com.bouthaina.sample.core.di.viewmodel.ViewModelModule
import com.bouthaina.sample.features.authordetails.AuthorDetailsFragment
import com.bouthaina.sample.features.authors.AuthorsFragment
import com.bouthaina.sample.core.navigation.RouteActivity
import dagger.Component
import javax.inject.Singleton

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

@Singleton
@Component(modules = [ApplicationModule::class, ViewModelModule::class])
interface ApplicationComponent {
    fun inject(application: AndroidApplication)
    fun inject(routeActivity: RouteActivity)

    fun inject(authorsFragment: AuthorsFragment)
    fun inject(authorDetailsFragment: AuthorDetailsFragment)
}
