
package com.bouthaina.sample.features.authordetails

import com.bouthaina.sample.AndroidTest
import com.bouthaina.sample.core.data.model.Address
import com.bouthaina.sample.core.data.model.Author
import com.bouthaina.sample.core.functional.Either
import com.bouthaina.sample.features.authors.AuthorsViewModel
import com.bouthaina.sample.features.authors.GetAuthors
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.given
import kotlinx.coroutines.experimental.runBlocking
import org.amshove.kluent.shouldEqualTo
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

class AuthorsViewModelTest : AndroidTest() {

    private lateinit var authorsViewModel: AuthorsViewModel
    @Mock private lateinit var address: Address
    @Mock private lateinit var author1: Author
    @Mock private lateinit var author2: Author

    @Mock
    private lateinit var getAuthor: GetAuthors

    @Before
    fun setUp() {
        address.longitude = "111"
        address.latitude = "558"
        author1.address = address
        author1.id = 1
        author1.name = "Roberto"
        author1.email = "jjgg@ggg.com"

        author2.address = address
        author2.id = 2
        author2.name = "Bouthaina"
        author2.email = "jjgg@ggg.com"

        authorsViewModel = AuthorsViewModel(getAuthor)
    }

    @Test fun `loading authors should update live data`() {
        val authorsList = listOf(author1, author2)
        given { runBlocking { getAuthor.run(eq(any())) } }.willReturn(Either.Right(authorsList))

        authorsViewModel.authors.observeForever {
            it!!.size shouldEqualTo 2
            it[0].id shouldEqualTo 1
            it[0].name shouldEqualTo "Roberto"
            it[1].id shouldEqualTo 2
            it[1].name shouldEqualTo "Bouthaina"
        }

        runBlocking { authorsViewModel.loadAuthors() }
    }
}