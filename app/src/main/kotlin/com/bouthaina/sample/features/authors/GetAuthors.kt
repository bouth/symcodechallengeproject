
package com.bouthaina.sample.features.authors

import com.bouthaina.sample.core.interactor.UseCase
import com.bouthaina.sample.core.interactor.UseCase.None
import com.bouthaina.sample.core.data.model.Author
import com.bouthaina.sample.core.data.network.AuthorsRepository
import javax.inject.Inject

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

class GetAuthors
@Inject constructor(private val authorsRepository: AuthorsRepository) : UseCase<List<Author>, None>() {

    override suspend fun run(params: None) = authorsRepository.authors()
}
