package com.bouthaina.sample.features.authordetails

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import com.bouthaina.sample.core.platform.BaseFragment
import com.bouthaina.sample.R
import com.bouthaina.sample.core.data.PostView
import com.bouthaina.sample.core.extension.loadUrlAndPostponeEnterTransition
import kotlinx.android.synthetic.main.fragment_post_details.*
import java.text.ParseException


/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class PostDetailsFragment : BaseFragment() {
    var selectedPostView = PostView(0, "", "", "", "", 0)


    companion object {
        private const val PARAM_POST = "param_post"
    }

    override fun layoutId() = R.layout.fragment_post_details

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bundle = arguments
        if (bundle != null) {
            selectedPostView = arguments!!.getParcelable<PostView>(PARAM_POST)
            imagePost.loadUrlAndPostponeEnterTransition(selectedPostView.imageUrl, this.activity!!)
            postTitle.text = selectedPostView.title
            postDate.text = getCurrentDate(selectedPostView.date)
            postComment.text = selectedPostView.body
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun getCurrentDate(date :String):String? {

        val fmt = java.text.SimpleDateFormat("yyyy-MM-dd")
        val fmt2 = java.text.SimpleDateFormat("dd-MM-yyyy" + "  hh:mm")

        return try { fmt2.format(fmt.parse(date)) }
        catch(pe : ParseException) { "" }

    }

    override fun onBackPressed() {

    }
}