package com.bouthaina.sample.core.data.model

import android.os.Parcel
import android.os.Parcelable
import com.bouthaina.sample.core.extension.empty

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

data class Address(var latitude: String, var longitude: String) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(latitude)
        parcel.writeString(longitude)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Address> {
        override fun createFromParcel(parcel: Parcel): Address {
            fun empty() = Address(String.empty(), String.empty())
            return Address(parcel)
        }

        override fun newArray(size: Int): Array<Address?> {
            return arrayOfNulls(size)
        }
    }
}
