
package com.bouthaina.sample.features.authordetails

import com.bouthaina.sample.UnitTest
import com.bouthaina.sample.core.data.model.*
import com.bouthaina.sample.core.data.network.AuthorsRepository.Network
import com.bouthaina.sample.core.exception.Failure.NetworkConnection
import com.bouthaina.sample.core.exception.Failure.ServerError
import com.bouthaina.sample.core.functional.Either
import com.bouthaina.sample.core.functional.Either.Right
import com.bouthaina.sample.core.data.network.AuthorsRepository
import com.bouthaina.sample.core.data.network.AuthorsService
import com.bouthaina.sample.core.platform.NetworkHandler
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyZeroInteractions
import org.amshove.kluent.shouldBeInstanceOf
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.internal.matchers.Any
import retrofit2.Call
import retrofit2.Response

class AuthorsRepositoryTest : UnitTest() {

    private lateinit var networkRepository: AuthorsRepository.Network

    @Mock private lateinit var networkHandler: NetworkHandler
    @Mock private lateinit var service: AuthorsService

    @Mock private lateinit var authorsCall: Call<List<AuthorEntity>>
    @Mock private lateinit var authorsResponse: Response<List<AuthorEntity>>
    @Mock private lateinit var postsCall: Call<List<PostEntity>>
    @Mock private lateinit var postsResponse: Response<List<PostEntity>>

    @Before fun setUp() {
        networkRepository = Network(networkHandler, service)
    }

    @Test fun `should return empty list by default`() {
        given { networkHandler.isConnected }.willReturn(true)
        given { authorsResponse.body() }.willReturn(null)
        given { authorsResponse.isSuccessful }.willReturn(true)
        given { authorsCall.execute() }.willReturn(authorsResponse)
        given { service.authors() }.willReturn(authorsCall)

        val authors = networkRepository.authors()
        authors shouldEqual Right(emptyList<Author>())
        verify(service).authors()
    }

    @Test fun `should get author list from service`() {
        given { networkHandler.isConnected }.willReturn(true)
        val adress = Address("555","555")
        given { authorsResponse.body() }.willReturn(listOf(AuthorEntity(1, "name","username","ff@gmial.com","httphgg",adress)))
        given { authorsResponse.isSuccessful }.willReturn(true)
        given { authorsCall.execute() }.willReturn(authorsResponse)
        given { service.authors() }.willReturn(authorsCall)

        val authors = networkRepository.authors()

        authors shouldEqual Right(listOf(Author(1, "name","username","ff@gmial.com","httphgg",adress)))
        verify(service).authors()
    }

    @Test fun `authors service should return network failure when no connection`() {
        given { networkHandler.isConnected }.willReturn(false)

        val authors = networkRepository.authors()

        authors shouldBeInstanceOf Either::class.java
        authors.isLeft shouldEqual true
        authors.either({ failure -> failure shouldBeInstanceOf NetworkConnection::class.java }, {})
        verifyZeroInteractions(service)
    }

    @Test fun `authors service should return network failure when undefined connection`() {
        given { networkHandler.isConnected }.willReturn(null)

        val authors = networkRepository.authors()

        authors shouldBeInstanceOf Either::class.java
        authors.isLeft shouldEqual true
        authors.either({ failure -> failure shouldBeInstanceOf NetworkConnection::class.java }, {})
        verifyZeroInteractions(service)
    }

    @Test fun `authors service should return server error if no successful response`() {
        given { networkHandler.isConnected }.willReturn(true)

        val authors = networkRepository.authors()

        authors shouldBeInstanceOf Either::class.java
        authors.isLeft shouldEqual true
        authors.either({ failure -> failure shouldBeInstanceOf ServerError::class.java }, {})
    }

    @Test fun `authors request should catch exceptions`() {
        given { networkHandler.isConnected }.willReturn(true)

        val authors = networkRepository.authors()

        authors shouldBeInstanceOf Either::class.java
        authors.isLeft shouldEqual true
        authors.either({ failure -> failure shouldBeInstanceOf ServerError::class.java }, {})
    }

    @Test fun `author details service should return network failure when no connection`() {
        given { networkHandler.isConnected }.willReturn(false)

        val posts = networkRepository.posts(1)

        posts shouldBeInstanceOf Either::class.java
        posts.isLeft shouldEqual true
        posts.either({ failure -> failure shouldBeInstanceOf NetworkConnection::class.java }, {})
        verifyZeroInteractions(service)
    }

    @Test fun `author details service should return network failure when undefined connection`() {
        given { networkHandler.isConnected }.willReturn(null)

        val posts = networkRepository.posts(1)

        posts shouldBeInstanceOf Either::class.java
        posts.isLeft shouldEqual true
        posts.either({ failure -> failure shouldBeInstanceOf NetworkConnection::class.java }, {})
        verifyZeroInteractions(service)
    }

    @Test fun `author details service should return server error if no successful response`() {
        given { networkHandler.isConnected }.willReturn(true)

        val posts = networkRepository.posts(1)

        posts shouldBeInstanceOf Either::class.java
        posts.isLeft shouldEqual true
        posts.either({ failure -> failure shouldBeInstanceOf ServerError::class.java }, {})
    }

    @Test fun `author details request should catch exceptions`() {
        given { networkHandler.isConnected }.willReturn(true)

        val posts = networkRepository.posts(1)

        posts shouldBeInstanceOf Either::class.java
        posts.isLeft shouldEqual true
        posts.either({ failure -> failure shouldBeInstanceOf ServerError::class.java }, {})
    }
}