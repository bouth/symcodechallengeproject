
package com.bouthaina.sample

import android.app.Application
import com.bouthaina.sample.core.di.ApplicationComponent
import com.bouthaina.sample.core.di.ApplicationModule
import com.bouthaina.sample.core.di.DaggerApplicationComponent
import com.squareup.leakcanary.BuildConfig
import com.squareup.leakcanary.LeakCanary

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

class AndroidApplication : Application() {

    val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        DaggerApplicationComponent
                .builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        this.injectMembers()
        this.initializeLeakDetection()
    }

    private fun injectMembers() = appComponent.inject(this)

    private fun initializeLeakDetection() {
        if (BuildConfig.DEBUG) LeakCanary.install(this)
    }
}
