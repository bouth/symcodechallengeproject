
package com.bouthaina.sample.core.navigation

import android.app.Activity
import android.content.Context
import android.os.Bundle

import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.view.View
import android.widget.ImageView
import com.bouthaina.sample.R

import com.bouthaina.sample.features.authordetails.AuthorDetailsActivity
import com.bouthaina.sample.core.data.AuthorView
import com.bouthaina.sample.core.data.PostView
import com.bouthaina.sample.core.platform.BaseActivity
import com.bouthaina.sample.core.platform.BaseFragment
import com.bouthaina.sample.features.authordetails.AuthorDetailsFragment
import com.bouthaina.sample.features.authors.AuthorsActivity

import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

@Singleton
class Navigator
@Inject constructor() {

    fun showMain(context: Context) = context.startActivity(AuthorsActivity.callingIntent(context))

    fun showAuthorDetails(activity: FragmentActivity, author: AuthorView, navigationExtras: Extras) {
        val intent = AuthorDetailsActivity.callingIntent(activity, author)
        val sharedView = navigationExtras.transitionSharedElement as ImageView
        val activityOptions = ActivityOptionsCompat
              .makeSceneTransitionAnimation(activity, sharedView, sharedView.transitionName)
        activity.startActivity(intent, activityOptions.toBundle())
    }

    fun navigateToNextFragment(postView: PostView, flag : String , currentFragment : Fragment, context: BaseFragment){

        val bundle = Bundle()
        bundle.putParcelable(flag, postView)
        val fragment = currentFragment
        fragment.arguments = bundle

        val fragmentManager = context.fragmentManager
        val fragmentTransaction = fragmentManager!!.beginTransaction()
        fragmentTransaction.replace(R.id.fragmentContainer,fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }
    class Extras(val transitionSharedElement: View)
}
