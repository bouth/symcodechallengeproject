
package com.bouthaina.sample.features.authors

import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.View
import com.bouthaina.sample.core.platform.BaseFragment
import com.bouthaina.sample.R
import com.bouthaina.sample.core.data.AuthorView
import com.bouthaina.sample.core.data.network.AuthorFailure.ListNotAvailable
import com.bouthaina.sample.core.exception.Failure
import com.bouthaina.sample.core.exception.Failure.NetworkConnection
import com.bouthaina.sample.core.exception.Failure.ServerError
import com.bouthaina.sample.core.extension.failure
import com.bouthaina.sample.core.extension.invisible
import com.bouthaina.sample.core.extension.observe
import com.bouthaina.sample.core.extension.viewModel
import com.bouthaina.sample.core.extension.visible
import com.bouthaina.sample.core.navigation.Navigator
import kotlinx.android.synthetic.main.fragment_authors.*
import javax.inject.Inject

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

class AuthorsFragment : BaseFragment() {

    @Inject lateinit var navigator: Navigator
    @Inject lateinit var authorsAdapter: AuthorsAdapter

    private lateinit var authorsViewModel: AuthorsViewModel

    override fun layoutId() = R.layout.fragment_authors

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)

        authorsViewModel = viewModel(viewModelFactory) {
            observe(authors, ::renderAuthorsList)
            failure(failure, ::handleFailure)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeView()
        loadAuthorsList()
    }

    private fun initializeView() {
        authorList.layoutManager = StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)
        authorList.adapter = authorsAdapter
        authorsAdapter.clickListener = { author, navigationExtras ->
                    navigator.showAuthorDetails(activity!!, author, navigationExtras) }
    }

    private fun loadAuthorsList() {
        emptyView.invisible()
        authorList.visible()
        showProgress()
        authorsViewModel.loadAuthors()
    }

    private fun renderAuthorsList(authors: List<AuthorView>?) {
        authorsAdapter.collection = authors.orEmpty()
        hideProgress()
    }

    private fun handleFailure(failure: Failure?) {
        when (failure) {
            is NetworkConnection -> renderFailure(R.string.failure_network_connection)
            is ServerError -> renderFailure(R.string.failure_server_error)
            is ListNotAvailable -> renderFailure(R.string.failure_authors_list_unavailable)
        }
    }

    private fun renderFailure(@StringRes message: Int) {
        authorList.invisible()
        emptyView.visible()
        hideProgress()
        notifyWithAction(message, R.string.action_refresh, ::loadAuthorsList)
    }
}
