
package com.bouthaina.sample.core.data

import android.os.Build
import android.os.Parcel

import android.support.annotation.RequiresApi
import com.bouthaina.sample.core.platform.KParcelable
import com.bouthaina.sample.core.platform.parcelableCreator

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

@Suppress("JAVA_CLASS_ON_COMPANION")
data class AuthorView(val id: Int, val name: String, val userName: String, val email: String,
                      val avatarUrl: String, val address: com.bouthaina.sample.core.data.model.Address) : KParcelable {
    companion object {
        @JvmField val CREATOR = parcelableCreator(::AuthorView)
    }

        @RequiresApi(Build.VERSION_CODES.M)
        constructor(parcel: Parcel) : this(parcel.readInt(), parcel.readString(), parcel.readString(),
                parcel.readString(), parcel.readString() ,parcel.readParcelable(com.bouthaina.sample.core.data.model.Address.javaClass.getClassLoader()))

    override fun writeToParcel(dest: Parcel, flags: Int) {
        with(dest) {
            writeInt(id)
            writeString(name)
            writeString(userName)
            writeString(email)
            writeString(avatarUrl)
            writeParcelable(address,0)
        }
    }
}
