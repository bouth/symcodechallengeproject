
#### master

https://gitlab.com/bouth/symcodechallengeproject

### ----------------------------------------------------------------------------------------------

# SymCodeChallengeProject  Android

An android  app for a micro blogging platform.

This list represents a high level user interactions available in the app:
   *On entry you're taken to the list of authors, where clicking on an author you get to the details screen

   *On the author details screen there is :

      -A header with the author's details available

      -A listing of all the posts written by this author (title, date, body ...)

    *Once you click a post in the list, you can see:

       -The post and detail about it

       -The list of comments about that post, which is ordered by date of creation, oldest on the top newest on the bottom Comments are not clickable

    *The needed API endpoints are described here: https://sym-json-server.herokuapp.com.


### ----------------------------------------------------------------------------------------------

## UI Layer: MVVM

## Clean Architecture

## Kotlin programming language

## Dagger 2 for dependency injection

## Retrofit2 for client APi consumption

## Unit test covered all the app using Mockito & espresso libs

## High code quality and reusability

## self design and usability

## Gitlab repository for code commits and review : https://gitlab.com/bouth/symcodechallengeproject


### ----------------------------------------------------------------------------------------------

## Developer details
* Bouthaina Touati
* Email : bouthainatt@gmail.com
* Personal git repository : https://gitlab.com/bouth
* Linkedin :https://www.linkedin.com/in/bouthaina-touati/

### ----------------------------------------------------------------------------------------------

