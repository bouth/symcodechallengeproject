
package com.bouthaina.sample.features.authordetails

import android.os.Bundle
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.View
import com.bouthaina.sample.core.platform.BaseFragment
import com.bouthaina.sample.core.data.AuthorView
import com.bouthaina.sample.core.data.model.Address

import com.bouthaina.sample.core.data.network.AuthorFailure.NonExistentAuthor
import com.bouthaina.sample.core.exception.Failure
import com.bouthaina.sample.core.exception.Failure.NetworkConnection
import com.bouthaina.sample.core.exception.Failure.ServerError
import com.bouthaina.sample.core.extension.close
import com.bouthaina.sample.core.extension.failure
import com.bouthaina.sample.core.extension.loadFromUrl
import com.bouthaina.sample.core.extension.loadUrlAndPostponeEnterTransition
import com.bouthaina.sample.core.extension.observe
import com.bouthaina.sample.core.extension.viewModel
import kotlinx.android.synthetic.main.fragment_author_details.*
import kotlinx.android.synthetic.main.toolbar.toolbar
import javax.inject.Inject

import kotlin.collections.ArrayList
import android.location.Geocoder
import com.bouthaina.sample.R
import com.bouthaina.sample.core.data.PostView
import com.bouthaina.sample.core.navigation.Navigator
import java.util.*
import java.io.IOException

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

class AuthorDetailsFragment : BaseFragment() {

    @Inject lateinit var authorDetailsAnimator: AuthorDetailsAnimator
    @Inject lateinit var postAdapter: PostAdapter
    @Inject lateinit var navigator: Navigator

    private lateinit var authorDetailsViewModel: AuthorDetailsViewModel

    override fun layoutId() = R.layout.fragment_author_details

    private var address = Address("112","555")
    private var currentAuthor = AuthorView(id = 0, name = "", userName = "", email = "", avatarUrl = "", address =address)


    companion object {
        private const val PARAM_AUTHOR = "param_author"
        private const val PARAM_POST = "param_post"

        fun forAuthor(author: AuthorView): AuthorDetailsFragment {
            val authorDetailsFragment = AuthorDetailsFragment()
            val arguments = Bundle()
            arguments.putParcelable(PARAM_AUTHOR, author)
            authorDetailsFragment.arguments = arguments

            return authorDetailsFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)
        activity?.let { authorDetailsAnimator.postponeEnterTransition(it) }

        authorDetailsViewModel = viewModel(viewModelFactory) {
            this@AuthorDetailsFragment.observe(postsList, ::renderAuthorDetails)
            failure(failure, ::handleFailure)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (firstTimeCreated(savedInstanceState)) {
            authorDetailsViewModel.loadAuthorDetails((arguments?.get(PARAM_AUTHOR) as AuthorView).id)
            currentAuthor = arguments?.get(PARAM_AUTHOR) as AuthorView

        } else {
            authorDetailsAnimator.cancelTransition(authorPoster)
            authorPoster.loadFromUrl((arguments!![PARAM_AUTHOR] as AuthorView).avatarUrl)
            currentAuthor = arguments!![PARAM_AUTHOR] as AuthorView
        }

        postLit.layoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        postLit.adapter = postAdapter


        postAdapter.clickListener = { post, navigationExtras ->
            navigator.navigateToNextFragment(post,PARAM_POST,PostDetailsFragment(),this) }
    }

    override fun onBackPressed() {
        authorDetailsAnimator.fadeInvisible(scrollView, authorDetails)
        authorDetailsAnimator.cancelTransition(authorPoster)
    }

    private fun renderAuthorDetails(post: List<PostView>?) {
        post.let {
            with(post) {
                activity?.let {
                    authorPoster.loadUrlAndPostponeEnterTransition(currentAuthor.avatarUrl, it)
                    it.toolbar.title = currentAuthor.name
                    val list = ArrayList<PostView>()
                    list.addAll(post!!)
                    list.sortByDescending { it.date }
                    postAdapter.collection = list

                }
                authorSummary.text = currentAuthor.email
                addressPost.text = getAdress(currentAuthor.address)

            }
        }
        authorDetailsAnimator.fadeVisible(scrollView, authorDetails)
    }


    private  fun getAdress(currentAdress : Address) :String{
        val geocoder = Geocoder(context, Locale.getDefault())
        var adress = ""
        try {
            val lat = java.lang.Double.parseDouble(currentAdress.latitude)
            val long =  java.lang.Double.parseDouble(currentAdress.longitude)
            adress = if(geocoder. getFromLocation(lat, long, 1).toString().equals("[]")) "No address is available " else geocoder. getFromLocation(lat, long, 1)[0].getAddressLine(0)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return adress
    }
    private fun handleFailure(failure: Failure?) {
        when (failure) {
            is NetworkConnection -> {
                notify(R.string.failure_network_connection); close()
            }
            is ServerError -> {
                notify(R.string.failure_server_error); close()
            }
            is NonExistentAuthor -> {
                notify(R.string.failure_author_non_existent); close()
            }
        }
    }
}
