
package com.bouthaina.sample.features.authors

import android.content.Context
import android.content.Intent
import com.bouthaina.sample.core.platform.BaseActivity

/**
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

class AuthorsActivity : BaseActivity() {

    companion object {
        fun callingIntent(context: Context) = Intent(context, AuthorsActivity::class.java)
    }

    override fun fragment() = AuthorsFragment()
}
